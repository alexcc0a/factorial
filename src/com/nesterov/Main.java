package com.nesterov;


public class Main {

    public static void main(String[] args) {
        int number = 6;
        long factorial = multiplyNumbers(number);
        System.out.println("Factorial of " + number + " = " + factorial);
    }
    public static long multiplyNumbers(int num) {
        if (num >= 1)
            return num * multiplyNumbers(num -1);
        else
            return 1;
    }
}